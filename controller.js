const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const port = process.argv.slice(2)[0];
const url = `localhost`
const app = express();

app.use(bodyParser.json());

const heroes = [
    { id: 1, name: 'Iron Man'},
    { id: 1, name: 'Captain America'},
    { id: 1, name: 'Spiderman'},
    { id: 1, name: 'Hulk'},
    { id: 1, name: 'Thor'},
    { id: 1, name: 'Vision'},
];

app.put('/send/', (req, res) => {
    heroes.forEach(heroe => {

        var options = {
            host: url,
            port: 8080,
            path: '/api/heroes/' + heroe.id,
            method: 'PUT',
            headers:{
                "Content-Type": "application/json"
            }
        };

        var post_req = http.request(options, function(res) {
            res.setEncoding('utf8');
            res.on('data', function (chunk) {
                console.log('Response: ' + chunk);
            });
        });
        post_req.write(JSON.stringify(heroe));
        post_req.end();
    });
});

console.log(`Controller service listening on port ${port}`);
app.listen(port);

